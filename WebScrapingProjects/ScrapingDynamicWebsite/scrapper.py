import requests

base_url = "https://unsplash.com/napi/search/photos?query=cat&per_page=20&page=3&xp=feedback-loop-v2%3Aexperiment"

r = requests.get(base_url)
data = r.json()

for item in data['results']:
    name = item['id']
    url = item['urls']['thumb']
    
    with open(name+".jpg", "wb") as f:
        f.write(requests.get(url).content)